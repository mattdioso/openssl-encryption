CC = gcc

all: clean encryption

clean:
	rm bin/main test/test.txt.enc

read:
	gcc -o bin/read_file read_file.c -I/usr/local/Cellar/openssl\@1.1/1.1.1k/include/ -L/usr/local/Cellar//openssl@1.1/1.1.1k/lib/ -lssl -lcrypto

metadata:
	gcc -o bin/metadata metadata.c

encryption:
	gcc -o bin/encryption encryption.c -I/usr/local/Cellar/openssl\@1.1/1.1.1k/include/ -L/usr/local/Cellar//openssl@1.1/1.1.1k/lib/ -lssl -lcrypto

main:
	gcc -o bin/main main.c -I/usr/local/Cellar/openssl\@1.1/1.1.1k/include/ -L/usr/local/Cellar//openssl@1.1/1.1.1k/lib/ -lssl -lcrypto

rand:
	gcc -o bin/rand rand.c -I/usr/local/Cellar/openssl\@1.1/1.1.1k/include/ -L/usr/local/Cellar//openssl@1.1/1.1.1k/lib/ -lssl -lcrypto

key:
	gcc -o bin/key key.c -I/usr/local/Cellar/openssl\@1.1/1.1.1k/include/ -L/usr/local/Cellar//openssl@1.1/1.1.1k/lib/ -lssl -lcrypto

size:
	gcc -o bin/size size.c -I/usr/local/Cellar/openssl\@1.1/1.1.1k/include/ -L/usr/local/Cellar//openssl@1.1/1.1.1k/lib/ -lssl -lcrypto

append:
	gcc -o bin/append append.c  -I/usr/local/Cellar/openssl\@1.1/1.1.1k/include/ -L/usr/local/Cellar//openssl@1.1/1.1.1k/lib/ -lssl -lcrypto

test:
	gcc -o bin/test test.c  -I/usr/local/Cellar/openssl\@1.1/1.1.1k/include/ -L/usr/local/Cellar//openssl@1.1/1.1.1k/lib/ -lssl -lcrypto

default: clean main