#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <string.h>

typedef struct metadata
{
    size_t key_len;
    size_t iv_len;
    unsigned char *key;
    unsigned char *iv;
    int iterations;
    const EVP_CIPHER *cipher_type;
} metadata_t;

void handleErrors(void);
int encrypt(unsigned char *key, unsigned char *iv, FILE *file, FILE *out_file, int encrypt_mode);
int decrypt(const metadata_t * meta, FILE *file, FILE *out_file, int encrypt_mode);

void write_header(FILE *file, const metadata_t *meta);
metadata_t *read_header(FILE * file) ;

int main(int argc, char *argv[])
{
    printf("starting\n");
    /*
     * Set up the key and iv. Do I need to say to not hard code these in a
     * real application? :-)
     */

    /* A 256 bit key */
    unsigned char *key =(unsigned char *)"01234567890123456789012345678901";

    /* A 128 bit IV */
    unsigned char *iv =(unsigned char *)"0123456789012345";

    /* Message to be encrypted */
    unsigned char *plaintext = NULL; //=
        //(unsigned char *)"The quick brown fox jumps over the lazy dog";

    FILE *file;
    FILE *out_file;
    size_t len;
    printf("opening file\n");
    file = fopen(argv[1], "rb");
    out_file = fopen(strcat(argv[1], ".enc"), "wb");
    printf("opened file\n");

    /*
     * Buffer for ciphertext. Ensure the buffer is long enough for the
     * ciphertext which may be longer than the plaintext, depending on the
     * algorithm and mode.
     */
    unsigned char ciphertext[128];

    /* Buffer for the decrypted text */
    unsigned char decryptedtext[128];

    int decryptedtext_len, ciphertext_len;
    metadata_t *metadata = (metadata_t *)malloc(sizeof(metadata_t));
    metadata->key_len = strlen((const char *)key);
    metadata->iv_len = strlen((const char *)iv);
    metadata->key = key;
    metadata->iv = iv;
    metadata->iterations = 1;
    metadata->cipher_type = EVP_aes_256_cbc();
    //printf("%lu\n", sizeof(metadata));
    //metadata_t *metadata = read_header(file);
    /*
     * Provide the message to be encrypted, and obtain the encrypted output.
     * EVP_EncryptUpdate can be called multiple times if necessary
     */
    //len = sizeof(metadata);
    //write_header(out_file, metadata);
    /* Encrypt the plaintext */
    /* 0 for decrypt, 1 for encrypt */
    int encrypt_mode = 1;
    // key = &metadata->key;
    // iv = &metadata->iv;
     //printf("%s\n", &metadata->key);
    //ciphertext_len = encrypt(key, iv, file, out_file, encrypt_mode);

    /* Do something useful with the ciphertext here */
    printf("Ciphertext is:\n");
    //BIO_dump_fp (stdout, (const char *)ciphertext, ciphertext_len);

    /* Decrypt the ciphertext */
    decryptedtext_len = decrypt(metadata, file, out_file, 0);

    /* Add a NULL terminator. We are expecting printable text */
    //decryptedtext[decryptedtext_len] = '\0';

    /* Show the decrypted text */
    //printf("Decrypted text is:\n");
    //printf("%s\n", decryptedtext);
    fclose(file);
    fclose(out_file);
    //free(metadata);

    return 0;
}

void handleErrors(void)
{
    ERR_print_errors_fp(stderr);
    abort();
}

int encrypt(unsigned char *key, unsigned char *iv, FILE *file, FILE *out_file, int encrypt_mode)
{
    EVP_CIPHER_CTX *ctx;

    int len;

    int ciphertext_len;
    int bytes_read;
     metadata_t *metadata;

    unsigned char in_buf[BUFSIZ], out_buf[BUFSIZ + EVP_CIPHER_block_size(EVP_aes_256_cbc()) + sizeof(metadata)], metadata_buf[sizeof(metadata_t)];
    
    /* Create and initialise the context */
    if (!(ctx = EVP_CIPHER_CTX_new()))
        handleErrors();
    printf("created new cipher\n");
    printf("%s\n", key);
    printf("%s\n", iv);
    /*
     * Initialise the encryption operation. IMPORTANT - ensure you use a key
     * and IV size appropriate for your cipher
     * In this example we are using 256 bit AES (i.e. a 256 bit key). The
     * IV size for *most* modes is the same as the block size. For AES this
     * is 128 bits
     */
    if (1 != EVP_CipherInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv, encrypt_mode))
        handleErrors();
    //EVP_CIPHER_CTX_set_padding(ctx, 0);
    if (encrypt_mode == 1) {
        metadata = (metadata_t *)malloc(sizeof(metadata_t));
        if (!metadata)
        {
            printf("error allocating metadata");
            return errno;
        }
        metadata->key_len = strlen((const char *)key);
        metadata->iv_len = strlen((const char *)iv);
        metadata->key = key;
        metadata->iv = iv;
        metadata->iterations = 1;
        metadata->cipher_type = EVP_aes_256_cbc();
        printf("%zu\n", metadata->key_len);
        //len = sizeof(metadata);
        //write_header(out_file, metadata);
    } else {
        printf("skipping create metadata\n");
    }
    /*
     * Provide the message to be encrypted, and obtain the encrypted output.
     * EVP_EncryptUpdate can be called multiple times if necessary
     */
    //fseek(file, sizeof(metadata_t), SEEK_SET);
    //len = sizeof(metadata);
    while (1)
    {
        
        bytes_read = fread(in_buf, 1, BUFSIZ, file);
        if (ferror(file))
        {
            printf("error reading file");
            EVP_CIPHER_CTX_cleanup(ctx);
        }
        if (bytes_read <= 0)
            break;
        if (1 != EVP_CipherUpdate(ctx, out_buf, &len, in_buf, bytes_read))
            handleErrors();
        fwrite(out_buf, 1, len, out_file);
        printf("LEN: %d\n", bytes_read);
        
        
        //ciphertext_len += len;
    }
    

    /*
     * Finalise the encryption. Further ciphertext bytes may be written at
     * this stage.
     */
    printf("BEFORE FINAL SIZE: %d\n", len);
    //len += sizeof(metadata);
    printf("AFTER FINAL SIZE: %d\n", len);
    if (1 != EVP_CipherFinal_ex(ctx, out_buf, &len))
        handleErrors();
    //ciphertext_len += len;
    fwrite(out_buf, sizeof(unsigned char), len, out_file);
    /* Clean up */
    EVP_CIPHER_CTX_cleanup(ctx);
    free(metadata);
    return ciphertext_len;
}

int decrypt(const metadata_t * meta, FILE *file, FILE *out_file, int encrypt_mode)
{
    EVP_CIPHER_CTX *ctx;

    int len;

    int ciphertext_len;
    int bytes_read;
    metadata_t *metadata;

    unsigned char in_buf[BUFSIZ], out_buf[BUFSIZ + EVP_CIPHER_block_size(EVP_aes_256_cbc())], metadata_buf[sizeof(metadata_t)];
    
    /* Create and initialise the context */
    if (!(ctx = EVP_CIPHER_CTX_new()))
        handleErrors();
    printf("created new cipher\n");
    printf("%s\n", &meta->key);
    printf("%s\n", &meta->iv);

    unsigned char * key = &meta->key;
    unsigned char * iv = &meta->iv;

    /*
     * Initialise the encryption operation. IMPORTANT - ensure you use a key
     * and IV size appropriate for your cipher
     * In this example we are using 256 bit AES (i.e. a 256 bit key). The
     * IV size for *most* modes is the same as the block size. For AES this
     * is 128 bits
     */
    if (1 != EVP_CipherInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv, encrypt_mode))
        handleErrors();
    //EVP_CIPHER_CTX_set_padding(ctx, 0);
    if (encrypt_mode == 1) {
        metadata = (metadata_t *)malloc(sizeof(metadata_t));
        if (!metadata)
        {
            printf("error allocating metadata");
            return errno;
        }
        metadata->key_len = strlen((const char *)key);
        metadata->iv_len = strlen((const char *)iv);
        metadata->key = key;
        metadata->iv = iv;
        metadata->iterations = 1;
        metadata->cipher_type = EVP_aes_256_cbc();
        printf("%zu\n", metadata->key_len);
        len = sizeof(metadata);
        //write_header(out_file, metadata);
    } else {
        printf("skipping create metadata\n");
    }
    /*
     * Provide the message to be encrypted, and obtain the encrypted output.
     * EVP_EncryptUpdate can be called multiple times if necessary
     */
    //fseek(file, sizeof(metadata_t), SEEK_SET);
    //len = sizeof(metadata);
    while (1)
    {
        
        bytes_read = fread(in_buf, 1, BUFSIZ, file);
        if (ferror(file))
        {
            printf("error reading file");
            EVP_CIPHER_CTX_cleanup(ctx);
        }
        if (bytes_read <= 0)
            break;
        if (1 != EVP_CipherUpdate(ctx, out_buf, &len, in_buf, bytes_read))
            handleErrors();
        fwrite(out_buf, 1, len, out_file);
        
        
        
        //ciphertext_len += len;
    }
    
    //len+= sizeof(metadata);
    /*
     * Finalise the encryption. Further ciphertext bytes may be written at
     * this stage.
     */
    printf("BEFORE FINAL SIZE: %d\n", len);
    //len -= sizeof(metadata);
    printf("AFTER FINAL SIZE: %d\n", len);
    if (1 != EVP_CipherFinal_ex(ctx, out_buf, &len))
        handleErrors();
    ciphertext_len += len;
    fwrite(out_buf, sizeof(unsigned char), len, out_file);
    /* Clean up */
    EVP_CIPHER_CTX_cleanup(ctx);
    free(metadata);
    return ciphertext_len;
}

void write_header(FILE *file, const metadata_t *meta)
{
    printf("%zu\n", sizeof("aes_256"));
    //int test = 69;
    if (fwrite(&(meta->key_len), sizeof(meta->key_len), 1, file) != 1)
        printf("error writing key len\n");
    if (fwrite(&(meta->iv_len), sizeof(meta->iv_len), 1, file) != 1)
        printf("error writing iv len\n");
    if (fwrite(meta->key, strlen((const char *)meta->key), 1, file) != 1)
        printf("error writing key\n");
    if (fwrite(meta->iv, strlen((const char *)meta->iv), 1, file) != 1)
        printf("error writing key\n");
    if (fwrite(&(meta->iterations), sizeof(int), 1, file) != 1)
        printf("error writing key\n");
    if (fwrite("aes_256", strlen("aes_256"), 1, file) != 1)
        printf("error writing key\n");
}

metadata_t *read_header(FILE *file)
{
    fprintf(stdout, "reading header\n");
    metadata_t *meta = malloc(sizeof(metadata_t));
    if (fread(&(meta->key_len), sizeof(meta->key_len), 1, file) != 1)
        printf("error reading key\n");
    if (fread(&(meta->iv_len), sizeof(meta->iv_len), 1, file) != 1)
        printf("error reading key\n");

    printf("%zu\n", meta->iv_len);
    size_t key_size = meta->key_len;
    printf("%zu\n", key_size);
    if (fread(&meta->key, key_size, 1, file) != 1)
        printf("error reading key\n");
    if (fread(&meta->iv, meta->iv_len, 1, file) != 1)
        printf("error reading iv\n");
    // if (fread(&(meta->iterations), sizeof(meta->iterations), 1, file)!= 1)
    //     printf("error reading iterations\n");
    // if (fread(meta->cipher_type, sizeof(meta->cipher_type), 1, file) != 1)
    //     printf("error reading cipher type\n");
    printf("%s\n", &meta->key);
    fprintf(stdout, "returning metadata\n");
    return meta;
}