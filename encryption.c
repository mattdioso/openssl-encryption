#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <openssl/rand.h>
#include <string.h>

#define BUFSIZE 1024
#define AES_BLOCK_SIZE 16
#define AES_256_KEY_SIZE 32

struct metadata {
    size_t key_len;
    size_t iv_len;
    size_t cipher_len;
    unsigned char * key;
    unsigned char * iv;
    int iterations;
    unsigned char * cipher_type;
};

int encrypt(FILE * in_file, FILE * out_file);
int decrypt(FILE * in_file, FILE * out_file);
void handleErrors(void);
void write_header(FILE *file, const struct metadata * meta);
int read_header(FILE *file, struct metadata * meta);

int main(int argc, char *argv[]) {

    FILE *in_file;
    FILE *out_file;
    size_t len;
    struct metadata *metadata;

    in_file = fopen(argv[1], "rb");
    out_file = fopen(strcat(argv[1], ".enc"), "wb");

    int decryted_len, encrypted_len;

    /* A 256 bit key */
    //unsigned char *key =(unsigned char *)"01234567890123456789012345678901";

    /* A 128 bit IV */
    //unsigned char *iv =(unsigned char *)"0123456789012345";

    //int num = encrypt(in_file, out_file);
    printf("BEFORE: %p\n", &in_file);
    // metadata_t * meta = read_header(in_file);
    int dec = decrypt(in_file, out_file);
    //free(metadata);
    fclose(out_file);
    fclose(in_file);
}

int encrypt(FILE * in_file, FILE * out_file) {
    unsigned char in_buf[BUFSIZE], out_buf[BUFSIZE + EVP_MAX_BLOCK_LENGTH];
    int in_len, out_len, total_len, temp_len;
    EVP_CIPHER_CTX *ctx;
    
    
    
    // unsigned char * key = meta->key;
    // unsigned char * iv = meta->iv;
    /* A 256 bit key */
    //unsigned char *key =(unsigned char *)"01234567890123456789012345678901";
    unsigned char key[AES_256_KEY_SIZE];

    /* A 128 bit IV */
    //unsigned char *iv =(unsigned char *)"0123456789012345";
    unsigned char iv[AES_BLOCK_SIZE];

    if (!RAND_bytes(key, AES_256_KEY_SIZE) || !RAND_bytes(iv, AES_BLOCK_SIZE)) {
        fprintf(stdout, "ERROR: rand_bytes error");
        return 0;
    }

    BIO_dump_fp (stdout, (const char *)key, AES_256_KEY_SIZE);
    fprintf(stdout, "\n\n\n");
    BIO_dump_fp (stdout, (const char *)iv, AES_BLOCK_SIZE);

    struct metadata *meta = (struct metadata *)malloc(sizeof(struct metadata));
    meta->key_len = strlen((const char *)key);
    meta->iv_len = strlen((const char *)iv);
    fprintf(stdout, "%zu\n", meta->key_len);
    fprintf(stdout, "%zu\n", meta->iv_len);
    meta->cipher_len = strlen((const char *)"aes_256");
    meta->key = (unsigned char *)key;
    meta->iv = (unsigned char *)iv;
    meta->iterations = 1;
    meta->cipher_type = (unsigned char *)"aes_256";
    //fwrite(meta, 1, sizeof(meta), out_file);
    //memcpy(out_buf, meta, sizeof(meta));
    write_header(out_file, meta);

    ctx = EVP_CIPHER_CTX_new();
    if (1 != EVP_EncryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv))
        handleErrors();

    for (;;) {
        in_len = fread(in_buf, 1, BUFSIZE, in_file);
        if (in_len <= 0) {
            break;
        }
        if (!EVP_EncryptUpdate(ctx, out_buf, &out_len, in_buf, in_len)) {
            EVP_CIPHER_CTX_free(ctx);
            return 0;
        }
        fwrite(out_buf, 1, out_len, out_file);
        total_len += out_len;
    }
    if (!EVP_EncryptFinal_ex(ctx, out_buf, &out_len)) {
        EVP_CIPHER_CTX_free(ctx);
        return 0;
    }
    fwrite(out_buf, 1, out_len, out_file);
    
    printf("total LEN: %d\n", total_len);
    printf("out LEN: %d\n", out_len);
    EVP_CIPHER_CTX_free(ctx);
    
    //meta = NULL;
    free(meta);
    return 1;
}

int decrypt(FILE * in_file, FILE * out_file) {
    unsigned char in_buf[BUFSIZE], out_buf[BUFSIZE + EVP_MAX_BLOCK_LENGTH];
    // unsigned char * in_buf = NULL;
    // unsigned char * out_buf = NULL;
    int in_len, out_len, total_len;
    EVP_CIPHER_CTX *ctx;
    
    struct metadata  meta;// = malloc(sizeof(struct metadata));
    read_header(in_file, &meta);
    // printf("AFTER: %p\n", &in_file);
     //printf("SIZE OF META: %lu\n", sizeof(meta));
      //unsigned char * key = meta->key;
      //unsigned char * iv = meta->iv;
      printf("%s\n", meta.key);
      printf("%s\n", meta.iv);
     printf("%s\n", meta.cipher_type);
    //  char s;
    //  while ((s=fgetc(in_file))!=EOF)
    //     printf("%c", s);
     
    //fseek(in_file, sizeof(meta), SEEK_CUR);
    /* A 256 bit key */
    //unsigned char *key =(unsigned char *)"01234567890123456789012345678901";

    /* A 128 bit IV */
    //unsigned char *iv =(unsigned char *)"0123456789012345";
    unsigned char * key = meta.key;
    unsigned char * iv = meta.iv;
    ctx = EVP_CIPHER_CTX_new();
    if (1 != EVP_DecryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv))
        handleErrors();

    for (;;) {
        
        in_len = fread(in_buf, 1, BUFSIZE, in_file);
        //printf("%s\n", in_buf);
        if (in_len <= 0) {
            break;
        }
        
        if (!EVP_DecryptUpdate(ctx, out_buf, &out_len, in_buf, in_len)) {
            EVP_CIPHER_CTX_free(ctx);
            return 0;
        }
        fwrite(out_buf, 1, out_len, out_file);
        total_len += out_len;
    }
    
    if (!EVP_DecryptFinal_ex(ctx, out_buf, &out_len)) {
        handleErrors();
        EVP_CIPHER_CTX_free(ctx);
        return 0;
    }
    fwrite(out_buf, 1, out_len, out_file);
    printf("total LEN: %d\n", total_len);
    printf("out LEN: %d\n", out_len);
    EVP_CIPHER_CTX_free(ctx);
    //free(meta);
    return 1;
}

void handleErrors(void)
{
    ERR_print_errors_fp(stderr);
    abort();
}

void write_header(FILE *file, const struct metadata *meta)
{
    printf("%lu\n", strlen((const char *)meta->key));
    //int test = 69;
    if (fwrite(&(meta->key_len), sizeof(meta->key_len), 1, file) != 1)
        printf("error writing key len\n");
    if (fwrite(&(meta->iv_len), sizeof(meta->iv_len), 1, file) != 1)
        printf("error writing iv len\n");
    if (fwrite(&(meta->cipher_len), sizeof(meta->cipher_len), 1, file) != 1)
        printf("error writing iv len\n");
    if (fwrite(meta->key, strlen((const char *)meta->key), 1, file) != 1)
        printf("error writing key\n");
    if (fwrite(meta->iv, strlen((const char *)meta->iv), 1, file) != 1)
        printf("error writing key\n");
    if (fwrite(&(meta->iterations), sizeof(int), 1, file) != 1)
        printf("error writing key\n");
    if (fwrite(meta->cipher_type, strlen((const char *)meta->cipher_type), 1, file) != 1)
        printf("error writing key\n");

}

int read_header(FILE *file, struct metadata *meta)
{
    fprintf(stdout, "reading header\n");
    //struct metadata *meta = malloc(sizeof(struct metadata));
    if (fread(&(meta->key_len), sizeof(meta->key_len), 1, file) != 1)
        printf("error reading key\n");
    if (fread(&(meta->iv_len), sizeof(meta->iv_len), 1, file) != 1)
        printf("error reading key\n");
    if (fread(&(meta->cipher_len), sizeof(meta->cipher_len), 1, file) != 1)
        printf("error reading key\n");

    printf("%zu\n", meta->iv_len);
    size_t key_size = meta->key_len;
    printf("%zu\n", key_size);
    size_t iv_size = meta->iv_len;
    unsigned char key_buf[key_size +1];
    unsigned char iv_buf[meta->iv_len + 1];
    unsigned char cipher_buf[meta->cipher_len + 1];

    if (fread(&key_buf, 1, key_size, file) != key_size)
        printf("error reading key\n");
    key_buf[key_size] = '\0';
     if (fread(&iv_buf, 1, iv_size, file) != iv_size)
         printf("error reading iv\n");
    iv_buf[iv_size]='\0';
    if (fread(&(meta->iterations), sizeof(meta->iterations), 1, file)!= 1)
        printf("error reading iterations\n");
    if (fread(&cipher_buf, meta->cipher_len, 1, file) != 1)
        printf("error reading cipher type\n");
    cipher_buf[meta->cipher_len]='\0';

    BIO_dump_fp (stdout, (const char *)key_buf, AES_256_KEY_SIZE);
    fprintf(stdout, "\n\n\n");
    BIO_dump_fp (stdout, (const char *)iv_buf, AES_BLOCK_SIZE);

    meta->key = malloc(sizeof(key_buf));
    strncpy(meta->key, key_buf, sizeof(key_buf));
    meta->iv = malloc(sizeof(iv_buf));
    strncpy(meta->iv, iv_buf, sizeof(iv_buf));
    meta->cipher_type =  malloc(sizeof(cipher_buf));
    strncpy(meta->cipher_type, cipher_buf, sizeof(cipher_buf));
    // meta->key = (unsigned char *)key_buf;
    // meta->iv = (unsigned char *)iv_buf;
    // meta->cipher_type = (unsigned char *) cipher_buf;


    printf("%s\n", meta->key);
    printf("%s\n", meta->iv);
    printf("%d\n", meta->iterations);
    printf("%s\n", meta->cipher_type);
    fprintf(stdout, "returning metadata\n");
    //return meta;
    return 1;

}