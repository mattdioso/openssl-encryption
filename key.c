#include <openssl/rand.h>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <errno.h>
#include <stdarg.h>
#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <err.h>
#include <time.h>
#include <openssl/hmac.h>
#include <unistd.h>

#define SALT_LEN 128

void handleErrors(void);

int main() {
    char * pass = getpass("Password: ");//(unsigned char *)"password123";
    fprintf(stdout, "#### PASSWORD ####: %s\n", pass);
    int master_key_len = 256;
    unsigned char master_key[master_key_len];
    int iter = 69000;
    clock_t begin, end;
    

    unsigned char salt[SALT_LEN];
    unsigned char encryption_salt[SALT_LEN];
    unsigned char hmac_salt[SALT_LEN];
    if (!RAND_bytes(salt, SALT_LEN)) {
        fprintf(stdout, "ERROR: rand_bytes master_salt\n");
        return 1;
    }

    BIO_dump_fp(stdout, (const char *)salt, SALT_LEN);
    fprintf(stdout, "%lu\n", sizeof(salt));
    begin = clock();
    if (!PKCS5_PBKDF2_HMAC((const char *)pass, sizeof(pass), (const unsigned char *)salt, SALT_LEN, 
                            iter, EVP_sha256(), master_key_len, master_key)) {
        fprintf(stdout, "ERROR master key dervation");
        handleErrors();
    }
    end = clock();
    double time_taken = (double)(end-begin)/CLOCKS_PER_SEC;
    BIO_dump_fp(stdout, (const char *)master_key, master_key_len);
    printf("%d iterations took %f seconds to generate master key\n", iter, time_taken);


    //generating second salt
    if (!RAND_bytes(encryption_salt, SALT_LEN)) {
        fprintf(stdout, "ERROR: rand_bytes encryption_salt\n");
        return 1;
    }
    unsigned char encryption_key[master_key_len];
    //deriving encryption key
    if (!PKCS5_PBKDF2_HMAC((const char *)master_key, master_key_len, (const unsigned char *)encryption_salt, SALT_LEN, 
                            1, EVP_sha256(), master_key_len, encryption_key)) {
        fprintf(stdout, "ERROR master key dervation\n");
        handleErrors();
    }

    printf("\n\n\n");
    BIO_dump_fp(stdout, (const char *)encryption_key, master_key_len);

    if (!RAND_bytes(hmac_salt, SALT_LEN)) {
        fprintf(stdout, "ERROR: rand_bytes hmac_salt\n");
        handleErrors();
    }

    unsigned char hmac_key[master_key_len];
    //deriving HMAC key
    if (!PKCS5_PBKDF2_HMAC((const char *)master_key, master_key_len, (const unsigned char *)hmac_salt, SALT_LEN,
                            1, EVP_sha256(), master_key_len, hmac_key));

    /* Pass data (IV + encrypted data) instead of encryption_key and master_key_len */
    unsigned char hmac[master_key_len];
    if (!HMAC(EVP_sha256(), &hmac_key, master_key_len, (const unsigned char *)encryption_key, master_key_len, 
                &hmac, &master_key_len)) {
        fprintf(stdout, "ErROR HMAC\n");
        handleErrors();
    }
    printf("HMAC???\n");
    HMAC_CTX *ctx = HMAC_CTX_new();
    if (!HMAC_Init_ex(ctx, &hmac_key, master_key_len, EVP_sha256(), NULL)) {
        handleErrors();
    }

    size_t hmac_size = HMAC_size(ctx);
    printf("%lu\n", hmac_size);
    BIO_dump_fp(stdout, (const char *)hmac, master_key_len);
}

void handleErrors(void)
{
    ERR_print_errors_fp(stderr);
    abort();
}