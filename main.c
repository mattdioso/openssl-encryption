/*
    Author: Matt Dioso
    CSS 577
    Encryption Assignment
    4/22/2021
*/
#include <stdlib.h>
#include <stdio.h>
#include <dirent.h>
#include <stdarg.h>
#include <err.h>
#include <time.h>
#include <unistd.h>
#include <openssl/hmac.h>
#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <openssl/rand.h>
#include <getopt.h>
#include <string.h>

#define SALT_LEN 128
#define MASTER_KEY_LEN 256
#define BUFSIZE 1024

struct metadata {
    unsigned char * m_salt;
    unsigned char * e_salt;
    unsigned char * h_salt;
    int iterations;
    unsigned char cipher[3];
    unsigned char hash[3];
};

void handleErrors(void);
void gen_m_key(unsigned char * m_key, unsigned char * salt, EVP_MD * digest, int iter, char * pass);
void encryption(unsigned char * m_key, unsigned char * m_salt, int interations, char * alg, char * hash, char * filename, FILE *in_file);
void decryption(unsigned char * key, unsigned char * iv, char * alg, FILE * tmp_file, FILE * out_file);
void derive_key(unsigned char * m_key, unsigned char * salt, EVP_MD * digest, unsigned char * derived_key);
void dump_header(struct metadata * meta);
void gen_salts(unsigned char * salt);
int merge_files(FILE* in, FILE * temp);
int read_header(FILE *file, struct metadata * meta);
void write_header(FILE *file, const struct metadata *meta);
void usage(void);
EVP_CIPHER * get_cipher(char alg[3]);
EVP_MD * get_hash(char hash[3]);

int main(int argc, char * argv[]) {
    int encrypt_action = 1;
    char* filename;
    if (argc < 3) {
        usage();
        return 1;
    }
    int option;
    int errflg = 0;
    char alg[3];
    char hash[3];
    while ((option = getopt(argc, argv, ":edf:t:h:")) != -1) {
        switch (option) {
            case 'e':
                printf("will encrypt\n");
                encrypt_action = 1;
                break;
            case 'd':
                printf("will decrypt\n");
                encrypt_action = 0;
                break;
            case 'f':
                filename = optarg;
                break;
            case ':':
                errflg++;
                break;
            case 't':
                strncpy(alg, optarg, 2);
                alg[2] = '\0';
                break;
            case 'h':
                strncpy(hash, optarg, 2);
                break;
            case '?':
                printf("you're using it wrong, dummy\n");
                errflg++;
                usage();
                break;
        }
    }
    if (errflg) {
        usage();
        exit(2);
    }


    char * pass = getpass("Password: ");
    unsigned char m_key[MASTER_KEY_LEN];
    unsigned char m_salt[SALT_LEN];
    int iter = 69000;
    clock_t begin, end;
    FILE *in_file;
    FILE *out_file;

    if (encrypt_action ==1) {
        if (!RAND_bytes(m_salt, SALT_LEN)) {
            fprintf(stdout, "ERROR: rand_bytes master_salt\n");
            handleErrors();
        }

        EVP_MD * digest;
        digest = get_hash(hash);

        begin = clock();        
        gen_m_key(&m_key, &m_salt, digest, iter, pass);
        end = clock();
        double time_taken = (double)(end-begin)/CLOCKS_PER_SEC;
        printf("%d iterations took %f seconds to generate master key\n", iter, time_taken);

        in_file = fopen(filename, "rb");
        
        encryption(&m_key, &m_salt, iter, alg, hash, filename, in_file);
    } else {
        in_file= fopen(filename, "rb");
        //create a temp file for us to write temp data
        char * temp_filename = malloc(sizeof(char) * (strlen(filename) + 4) );
        strcpy(temp_filename, filename);
        strcat(temp_filename, ".tmp");
        FILE * temp_file = fopen(temp_filename, "wb+");
        struct metadata meta;
        read_header(in_file, &meta);
        //dump_header(&meta);
        
        EVP_CIPHER * cipher;
        cipher = get_cipher(meta.cipher);

        EVP_MD * digest;
        digest = get_hash(meta.hash);

        gen_m_key(&m_key, meta.m_salt, digest, meta.iterations, pass);
        
        unsigned char e_key[MASTER_KEY_LEN];
        unsigned char h_key[MASTER_KEY_LEN];
        int iv_len;
        //derive encryption key
        derive_key(m_key, meta.e_salt, digest, &e_key);

        //derive hmac key
        derive_key(m_key, meta.h_salt, digest, &h_key);

        //getting length of HMAC
        HMAC_CTX *hmac_ctx = HMAC_CTX_new();
        if (!HMAC_Init_ex(hmac_ctx, &h_key, MASTER_KEY_LEN, digest, NULL)) {
            HMAC_CTX_free(hmac_ctx);
            handleErrors();
        }
        size_t hmac_size = HMAC_size(hmac_ctx);
        
        unsigned char hmac[hmac_size];
        unsigned char hmac_gen[hmac_size];

        if (fread(hmac, 1, hmac_size, in_file)!= hmac_size) {
            fprintf(stdout, "error reading hmac");
        }
        hmac[hmac_size] ='\0';
        
        iv_len = EVP_CIPHER_iv_length(cipher);
        unsigned char iv[iv_len+1];
        if (fread(iv, 1, iv_len, in_file) != iv_len) {
            fprintf(stdout, "error reading iv\n");
        }
        iv[iv_len] = '\0';
        // printf("IV:\n\n");
        // BIO_dump_fp(stdout, (const char *)iv, iv_len);

        if (!HMAC_Update(hmac_ctx, (const unsigned char *)iv, iv_len)) {
            HMAC_CTX_free(hmac_ctx);
            fprintf(stdout, "ERROR IV HMAC\n");
            handleErrors();
        }

        unsigned char data[BUFSIZE];
        unsigned char out_buf[BUFSIZE + EVP_MAX_BLOCK_LENGTH];
        int out_len;
        //same process as earlier, except we're only generating HMAC for verification and writing encrpyted data out to temp file
        //can't decrypt until we verify HMAC
        for (;;) {
            int in_len = fread(data, 1, BUFSIZE, in_file);
            if (in_len<= 0) {
                break;
            }
            if (!HMAC_Update(hmac_ctx, (const unsigned char *)data, in_len)) {
                HMAC_CTX_free(hmac_ctx);
                fprintf(stdout, "ERROR DATA HMAC\n");
                handleErrors();
            }
            fwrite(data, 1, in_len, temp_file);
            
        }
        fwrite(out_buf, 1, out_len, temp_file);

        if (!HMAC_Final(hmac_ctx, hmac_gen, &hmac_size)) {
            HMAC_CTX_free(hmac_ctx);
            fprintf(stdout, "ERROR FINAL HMAC\n");
            handleErrors();
        }

        //compare HMACS
        if (memcmp(hmac, hmac_gen, hmac_size) == 0) {
            printf("HMACS MATCH\n");
            char * out_filename = malloc(sizeof(char) * strlen(filename));
            strcpy(out_filename, filename);
            strcat(out_filename, ".dec");
            out_file = fopen(out_filename, "wb");
            decryption((unsigned char *)e_key, (unsigned char *)iv, meta.cipher, temp_file, out_file);
            free(out_filename);
        } else {
            printf("HMACS DON'T MATCH\n");
        }
        //free(data_buf);
        int del = remove(temp_filename);
        if (!del) {
            printf("deleted temp file successfully\n");
        } else {
            printf("temp file wasn't deleted\n");
        }
        HMAC_CTX_free(hmac_ctx);
        free(temp_filename);
        fclose(temp_file);    
    }
    
    fclose(out_file);
    
    fclose(in_file);
}

void encryption(unsigned char * m_key, unsigned char * m_salt, int iterations, char * alg, char * hash, char * filename, FILE *in_file) {
    unsigned char data_buf[BUFSIZE], out_buf[BUFSIZE + EVP_MAX_BLOCK_LENGTH];
    int in_len, out_len;
    int total_len =0;
    unsigned char e_salt[SALT_LEN];
    unsigned char h_salt[SALT_LEN];
    unsigned char e_key[MASTER_KEY_LEN];
    unsigned char h_key[MASTER_KEY_LEN];
    int iv_len;
    char * data_string = malloc(sizeof(char) *BUFSIZE);
    size_t data_len = BUFSIZE;
    

    char * temp_filename = malloc(sizeof(char) * (strlen(filename) + 4));
    strcpy(temp_filename, filename);
    strcat(temp_filename, ".tmp");
    FILE * tempfile = fopen(temp_filename, "wb+");
    FILE * out_file = fopen(strcat(filename, ".enc"), "wb");

    EVP_CIPHER * cipher;
    cipher = get_cipher(alg);

    EVP_MD * digest;
    digest = get_hash(hash);

    //generate encryption salt
    gen_salts(&e_salt);

    //generate HMAC salt
    gen_salts(&h_salt);

    //derive encryption key
    derive_key(m_key, e_salt, digest, &e_key);

    //derive hmac key
    derive_key(m_key, h_salt, digest, &h_key);

    iv_len = EVP_CIPHER_iv_length(cipher);
    unsigned char iv[iv_len];
    if (!RAND_bytes(iv, iv_len)) {
        fprintf(stdout, "ERROR: iv gen error\n");
        handleErrors();
    }

    
    //getting length of HMAC
    HMAC_CTX *hmac_ctx = HMAC_CTX_new();
    if (!HMAC_Init_ex(hmac_ctx, &h_key, MASTER_KEY_LEN, digest, NULL)) {
        printf("hmac init error\n");
        HMAC_CTX_free(hmac_ctx);
        handleErrors();
    }
    size_t hmac_size = HMAC_size(hmac_ctx);
    unsigned char hmac[hmac_size];

    //BIO_dump_fp(stdout, (const char *)iv, iv_len);
    //initialize HMAC with IV 
    if (!HMAC_Update(hmac_ctx, (const unsigned char *)iv, iv_len)) {
        HMAC_CTX_free(hmac_ctx);
        fprintf(stdout, "ERROR IV HMAC\n");
        handleErrors();
    }
    
    EVP_CIPHER_CTX *ctx;
    ctx = EVP_CIPHER_CTX_new();
    //initialize encryption 
    if (1 != EVP_EncryptInit_ex(ctx, cipher, NULL, e_key, iv))
        handleErrors();

    // iteratively generating HMAC while encrypting the data at the same time.
    for (;;) {
        int bytes_read = fread(data_buf, 1, BUFSIZE, in_file);
        if (bytes_read <= 0) {
            break;
        }
        
        //order of encrypt/HMAC calls is important. need to make sure we generate the HMAC over encrypted data, not plaintext
        if (!EVP_EncryptUpdate(ctx, out_buf, &out_len, data_buf, bytes_read)) {
            EVP_CIPHER_CTX_free(ctx);
            handleErrors();
        }
        if (!HMAC_Update(hmac_ctx, (const unsigned char *)out_buf, out_len)) {
            HMAC_CTX_free(hmac_ctx);
            fprintf(stdout, "ERROR DATA HMAC\n");
            handleErrors();
        }
        //writing out encrypted data to temp file because we need to write headers first.
        fwrite(out_buf, 1, out_len, tempfile);
        
        total_len += out_len;
    }

    if (!EVP_EncryptFinal_ex(ctx, out_buf, &out_len)) {
        EVP_CIPHER_CTX_free(ctx);
        handleErrors();
    }
    total_len += out_len;
    fwrite(out_buf, 1, out_len, tempfile);
    
    if (!HMAC_Update(hmac_ctx, (const unsigned char *)out_buf, out_len)) {
        HMAC_CTX_free(hmac_ctx);
        fprintf(stdout, "ERROR DATA HMAC\n");
        handleErrors();
    }
    
    if (!HMAC_Final(hmac_ctx, hmac, &hmac_size)) {
        HMAC_CTX_free(hmac_ctx);
        fprintf(stdout, "ERROR FINAL HMAC\n");
        handleErrors();
    }
    
    struct metadata *meta = (struct metadata *)malloc(sizeof(struct metadata));
    meta->m_salt = (unsigned char *)m_salt;
    meta->e_salt = (unsigned char *)e_salt;
    meta->h_salt = (unsigned char *)h_salt;
    meta->iterations = iterations;
    strncpy(meta->cipher, alg, sizeof(meta->cipher));
    strncpy(meta->hash, hash, sizeof(meta->hash));

    /*
        File structure:
        Metadata
        HMAC
        IV
        Encrypted data
    */
    write_header(out_file, meta);
    dump_header(meta);
    fwrite(hmac, 1, hmac_size, out_file);
    fwrite(iv, 1, iv_len, out_file);

    //merge the temp file we generated earlier with our result file
    int merge_len = merge_files(out_file, tempfile);
    if (merge_len != total_len) {
        printf("%d\t%d\n", merge_len, total_len);
        printf("Error merging files\n");
        exit(2);
    }

    //delete the temp file
    int del = remove(temp_filename);
    if (!del) {
        printf("deleted temp file successfully\n");
    } else {
        printf("temp file wasn't deleted\n");
    }

    EVP_CIPHER_CTX_free(ctx);
    HMAC_CTX_free(hmac_ctx);
    free(data_string);
    free(temp_filename);
    fclose(out_file);
    fclose(tempfile);
    //return 1;
}

void decryption(unsigned char * key, unsigned char * iv, char * alg, FILE * tmp_file, FILE * out_file) {
    EVP_CIPHER_CTX *dec_ctx;
    unsigned char in_buf[BUFSIZE], out_buf[BUFSIZE + EVP_MAX_BLOCK_LENGTH];
    int out_len; 
    int total_len =0;
    printf("%s\n", alg);
    EVP_CIPHER * cipher;
    cipher = get_cipher(alg);

    //reset tmep file pointer to start
    fseek(tmp_file, 0, SEEK_SET);

    dec_ctx = EVP_CIPHER_CTX_new();
    if (1 != EVP_DecryptInit_ex(dec_ctx, cipher, NULL, key, iv)) {
        handleErrors();
        return;
    }
    
    //now read all encrypted data and decrypt it out to output file.
    for (;;) {
        int bytes_read = fread(in_buf, 1, BUFSIZE, tmp_file);
        if (bytes_read <= 0) {
            break;
        }
        if (!EVP_DecryptUpdate(dec_ctx, out_buf, &out_len, in_buf, bytes_read)) {
            EVP_CIPHER_CTX_free(dec_ctx);
            handleErrors();
            return;
        }
    
        fwrite(out_buf, 1, out_len, out_file);
        total_len += out_len;
    }
    
    if (!EVP_DecryptFinal_ex(dec_ctx, out_buf , &out_len)) {
        handleErrors();
        EVP_CIPHER_CTX_free(dec_ctx);
        return;
    }
    fwrite(out_buf, 1, out_len, out_file);
    EVP_CIPHER_CTX_free(dec_ctx);
}

int merge_files(FILE* in, FILE * temp) {
    
    fseek(temp, 0, SEEK_SET);
    char in_buf[BUFSIZE];
    int total_len = 0;
    for (;;) {
        int bytes_read = fread(in_buf, 1, BUFSIZE, temp);
        if (bytes_read <= 0) {
            break;
        }
        fwrite(in_buf, 1, bytes_read, in);
        total_len += bytes_read;
    }
    return total_len;
}

void derive_key(unsigned char * m_key, unsigned char * salt, EVP_MD * digest, unsigned char * derived_key) {
    if (!PKCS5_PBKDF2_HMAC((const char *)m_key, MASTER_KEY_LEN, (const unsigned char *)salt, SALT_LEN,
                            1, digest, MASTER_KEY_LEN, derived_key)) {
        fprintf(stdout, "ERROR encryption key derivation\n");
        handleErrors();
    }
}

void gen_salts(unsigned char * salt) {
    if (!RAND_bytes(salt, SALT_LEN)) {
        fprintf(stdout, "ERROR: rand_bytes salt\n");
        handleErrors();
    }
}

void write_header(FILE * file, const struct metadata * meta) {
    
    if (fwrite((meta->m_salt), SALT_LEN, 1, file) != 1)
        fprintf(stdout, "error writing m_salt\n");
    if (fwrite((meta->e_salt), SALT_LEN, 1, file) != 1)
        fprintf(stdout, "error writing e_salt\n");
    if (fwrite((meta->h_salt), SALT_LEN, 1, file) != 1)
        fprintf(stdout, "error writing h_salt\n");
    if (fwrite(&(meta->iterations), sizeof(int), 1, file) != 1)
        fprintf(stdout, "error writing iterations\n");
    if (fwrite((meta->cipher), strlen(meta->cipher), 1, file) != 1)
        fprintf(stdout, "error writing cipher\n");
    if (fwrite(meta->hash, strlen(meta->hash), 1, file) != 1)
        fprintf(stdout, "error writing hash\n");
}

int read_header(FILE *file, struct metadata *meta)
{
    unsigned char m_salt[SALT_LEN +1];
    unsigned char e_salt[SALT_LEN+1];
    unsigned char h_salt[SALT_LEN+1];
    unsigned char cipher[3];
    unsigned char hash[3];
    int iterations;

    if (fread(&m_salt, 1, SALT_LEN, file) != SALT_LEN)
        printf("error reading m_salt\n");
    m_salt[SALT_LEN] = '\0';
    
    if (fread(&e_salt, 1, SALT_LEN, file) != SALT_LEN) {
        printf("error reading e_salt\n");
    }
    e_salt[SALT_LEN] = '\0';
    
    if (fread(&h_salt, 1, SALT_LEN, file) != SALT_LEN) {
        printf("error reading e_salt\n");
    }
    h_salt[SALT_LEN] = '\0';
    if (fread(&iterations, sizeof(iterations), 1, file) != 1) {
        printf("error reading iterations\n");
    }

    if (fread(&cipher, 1, 2, file) != 2) {
        printf("error reading cipher\n");
    }
    cipher[2] = '\0';
    if (fread(&hash, 1, 2, file) != 2) {
        printf("error reading hash\n");
    }
    hash[2] = '\0';
    

    meta->m_salt = malloc(SALT_LEN);
    meta->e_salt = malloc(SALT_LEN);
    meta->h_salt = malloc(SALT_LEN);
    
    memcpy(meta->m_salt, m_salt, SALT_LEN);
    memcpy(meta->e_salt, e_salt, SALT_LEN);
    memcpy(meta->h_salt, h_salt, SALT_LEN);
    strncpy(meta->cipher, cipher, strlen(cipher));
    strncpy(meta->hash, hash, strlen(hash));
    meta->iterations = iterations;
    
    return 1;
}

void dump_header(struct metadata * meta) {
    printf("m_salt\n\n");
    BIO_dump_fp(stdout, (const char *)meta->m_salt, SALT_LEN);
    printf("e_salt\n\n");
    BIO_dump_fp(stdout, (const char *)meta->e_salt, SALT_LEN);
    printf("h_salt\n\n");
    BIO_dump_fp(stdout, (const char *)meta->h_salt, SALT_LEN);
    printf("iterations\n");
    printf("%d\n", meta->iterations);
    printf("cipher\n");
    printf("%s\n", meta->cipher);
    printf("hash\n");
    printf("%s\n", meta->hash);
}

void gen_m_key(unsigned char * m_key, unsigned char * salt, EVP_MD * digest, int iter, char * pass) {
    
    if (!PKCS5_PBKDF2_HMAC((const char *)pass, sizeof(pass), (const unsigned char *)salt, SALT_LEN,
                            iter, digest, MASTER_KEY_LEN, m_key)) {
        fprintf(stdout, "ERROR on master key derivation");
        handleErrors();
    }
    
}

EVP_CIPHER * get_cipher(char alg[3]) {
    if (strcmp(alg, "a1") == 0) {
        printf("using aes128\n");
        return EVP_aes_128_cbc();
    } else if (strcmp(alg, "a2") == 0) {
        printf("using aes256\n");
        return EVP_aes_128_cbc();
    } else if (strcmp(alg, "3d") == 0) {
        printf("using 3des\n");
        return EVP_des_ede3_cbc();
    } else {
        printf("using aes256\n");
        return EVP_aes_256_cbc();
    }
}

EVP_MD * get_hash(char hash[3]) {
    if (strcmp(hash, "s2") == 0) {
        printf("using sha256 hash\n");
        return EVP_sha256();
    } else if (strcmp(hash, "s5") == 0) {
        printf("using sha512 hash\n");
        return EVP_sha512();
    } else {
        printf("using sha256 hash\n");
        return EVP_sha256();
    }
}

void handleErrors(void)
{
    ERR_print_errors_fp(stderr);
    abort();
}

void usage(void) {
    printf("usage: main -e/-d -f <path to text file> -t <encryption type>\n");
    printf("-e\t\tEnables encryption\n");
    printf("-d\t\tEnables decryption\n");
    printf("-f\t\tInput file\n");
    printf("-t\t\tSpecifies Encryption type. a1 for AES_128, a2 for AES_256, 3d for 3DES. Default is AES_256\n");
    printf("-h\t\tDisplay help text\n");
}