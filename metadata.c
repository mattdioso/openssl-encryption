#include <dirent.h>
#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

enum { MAX_FILES = 5 };

struct files
{
    char *file_name;
    int file_size;
};

typedef struct file_header
{
    int file_count;
    struct files file[MAX_FILES];
} metadata;

static void err_exit(const char *format, ...);
static metadata *create_header(const char *directory);
static void release_header(metadata *header);
static void write_header(FILE *fp, const metadata *header);
static metadata *read_header(FILE *fp);
static void dump_header(FILE *fp, const char *tag, const metadata *header);

int main(int argc, char **argv)
{
    if (argc != 3)
        err_exit("Usage: %s file directory\n", argv[0]);

    const char *name = argv[1];
    const char *path = argv[2];
    FILE *fp = fopen(name, "wb");

    if (fp == 0)
        err_exit("Failed to open file %s for writing (%d: %s)\n", name, errno, strerror(errno));

    metadata *header = create_header(path);
    dump_header(stdout, "Data to be written", header);
    write_header(fp, header);
    fclose(fp);                     // Ignore error on close
    release_header(header);

    if ((fp = fopen(name, "rb")) == 0)
        err_exit("Failed to open file %s for reading (%d: %s)\n", name, errno, strerror(errno));

    metadata *read_info = read_header(fp);
    dump_header(stdout, "Data as read", read_info);
    release_header(read_info);

    fclose(fp);                     // Ignore error on close
    return 0;
}

static metadata *create_header(const char *path)
{
    int file_count = 0;
    DIR * dirp = opendir(path);
    struct dirent * entry;
    if (dirp == 0)
        err_exit("Failed to open directory %s (%d: %s)\n", path, errno, strerror(errno));
    metadata *header = (metadata *)malloc(sizeof(metadata));
    if (header == 0)
        err_exit("Failed to malloc space for header (%d: %s)\n", errno, strerror(errno));

    header->file_count = 0;
    while ((entry = readdir(dirp)) != NULL && file_count < MAX_FILES)
    {
        // d_type is not portable - POSIX says you can only rely on d_name and d_ino
        if (entry->d_type == DT_REG)
        {   /* If the entry is a regular file */
            // Avoid off-by-one under-allocation by using strdup()
            header->file[file_count].file_name = strdup(entry->d_name);
            if (header->file[file_count].file_name == 0)
                err_exit("Failed to strdup() file %s (%d: %s)\n", entry->d_name, errno, strerror(errno));
            //Put static but i have logic for this i will apply later.
            header->file[file_count].file_size = 10;
            file_count++;
        }
    }
    header->file_count = file_count;
    closedir(dirp);
    //printf("File Count : %d\n", file_count);
    return header;
}

static void write_header(FILE *fp, const metadata *header)
{
    if (fwrite(&header->file_count, sizeof(header->file_count), 1, fp) != 1)
        err_exit("Write error on file count (%d: %s)\n", errno, strerror(errno));
    const struct files *files = header->file;
    for (int i = 0; i < header->file_count; i++)
    {
        unsigned short name_len = strlen(files[i].file_name) + 1;
        if (fwrite(&name_len, sizeof(name_len), 1, fp) != 1)
            err_exit("Write error on file name length (%d: %s)\n", errno, strerror(errno));
        if (fwrite(files[i].file_name, name_len, 1, fp) != 1)
            err_exit("Write error on file name (%d: %s)\n", errno, strerror(errno));
        if (fwrite(&files[i].file_size, sizeof(files[i].file_size), 1, fp) != 1)
            err_exit("Write error on file size (%d: %s)\n", errno, strerror(errno));
    }
}

static metadata *read_header(FILE *fp)
{
    metadata *header = malloc(sizeof(*header));
    if (header == 0)
        err_exit("Failed to malloc space for header (%d:%s)\n", errno, strerror(errno));
    if (fread(&header->file_count, sizeof(header->file_count), 1, fp) != 1)
        err_exit("Read error on file count (%d: %s)\n", errno, strerror(errno));
    struct files *files = header->file;
    for (int i = 0; i < header->file_count; i++)
    {
        unsigned short name_len;
        if (fread(&name_len, sizeof(name_len), 1, fp) != 1)
            err_exit("Read error on file name length (%d: %s)\n", errno, strerror(errno));
        files[i].file_name = malloc(name_len);
        if (files[i].file_name == 0)
            err_exit("Failed to malloc space for file name (%d:%s)\n", errno, strerror(errno));
        if (fread(files[i].file_name, name_len, 1, fp) != 1)
            err_exit("Read error on file name (%d: %s)\n", errno, strerror(errno));
        if (fread(&files[i].file_size, sizeof(files[i].file_size), 1, fp) != 1)
            err_exit("Read error on file size (%d: %s)\n", errno, strerror(errno));
    }
    return(header);
}

static void dump_header(FILE *fp, const char *tag, const metadata *header)
{
    const struct files *files = header->file;
    fprintf(fp, "Metadata: %s\n", tag);
    fprintf(fp, "File count: %d\n", header->file_count);
    for (int i = 0; i < header->file_count; i++)
        fprintf(fp, "File %d: size %5d, name %s\n", i, files[i].file_size, files[i].file_name);
}

static void release_header(metadata *header)
{
    for (int i = 0; i < header->file_count; i++)
    {
        /* Zap file name, and pointer to file name */
        memset(header->file[i].file_name, 0xDD, strlen(header->file[i].file_name)+1);
        free(header->file[i].file_name);
        memset(&header->file[i].file_name, 0xEE, sizeof(header->file[i].file_name));
    }
    free(header);
}

static void err_exit(const char *format, ...)
{
    va_list args;
    va_start(args, format);
    vfprintf(stderr, format, args);
    va_end(args);
    exit(EXIT_FAILURE);
}