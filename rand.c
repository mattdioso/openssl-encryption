#include <openssl/rand.h>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <errno.h>
#include <stdarg.h>
#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>

#define AES_BLOCK_SIZE 16
#define AES_256_KEY_SIZE 32

int main() {
    unsigned char key[AES_256_KEY_SIZE];
    unsigned char IV[AES_BLOCK_SIZE];

    if (!RAND_bytes(key, AES_256_KEY_SIZE) || !RAND_bytes(IV, AES_BLOCK_SIZE)) {
        fprintf(stdout, "ERROR: rand_bytes error");
        return 1;
    }
    fprintf(stdout, "generated\n");
    //fprintf(stdout, "%s\n", key);
    BIO_dump_fp (stdout, (const char *)key, AES_256_KEY_SIZE);
    fprintf(stdout, "\n\n\n");
    BIO_dump_fp (stdout, (const char *)IV, AES_BLOCK_SIZE);
    //PKCS5_PBKDF2_HMAC()
}