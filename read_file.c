#include <stdio.h>
#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <string.h>

typedef struct metadata {
    size_t key_len;
    size_t iv_len;
    unsigned char * key;
    unsigned char * iv;
    unsigned int iterations;
    const EVP_CIPHER * cipher_type;
} metadata_t;


int main(int argc, char* argv []) {
    FILE * file;
    file = fopen(argv[1], "rb");
    char * buffer;
       unsigned long file_len;
       fseek(file, 0, SEEK_END);
       file_len = ftell(file);

       buffer = malloc(file_len+1);
       int len = fread(buffer, file_len, 1, file);
       printf("%d\n", len);
       //BIO_dump_fp(stdout, buffer, file_len);
       free(buffer);
    fclose(file);
    return 0;
}

