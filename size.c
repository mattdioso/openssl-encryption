#include <openssl/rand.h>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <errno.h>
#include <stdarg.h>
#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <err.h>
#include <time.h>
#include <openssl/hmac.h>
#include <unistd.h>

void handleErrors(void);

int main() {
    printf("========= IV LENGTHS: ===========\n");
    int aes_256_iv_len, aes_128_iv_len, des_iv_len;
    aes_256_iv_len = EVP_CIPHER_iv_length(EVP_aes_256_cbc());
    aes_128_iv_len = EVP_CIPHER_iv_length(EVP_aes_128_cbc());
    des_iv_len = EVP_CIPHER_iv_length(EVP_des_ede3_cbc());

    printf("AES_128: %d\n", aes_128_iv_len);
    printf("AES_256: %d\n", aes_256_iv_len);
    printf("3DES: %d\n", des_iv_len);

    printf("========= BLOCK LENGTHS: ===========\n");
    int aes_256_block_size, aes_128_block_size, des_block_size;
    aes_256_block_size = EVP_CIPHER_block_size(EVP_aes_256_cbc());
    aes_128_block_size = EVP_CIPHER_block_size(EVP_aes_128_cbc());
    des_block_size = EVP_CIPHER_block_size(EVP_des_ede3_cbc());

    printf("AES_128: %d\n", aes_128_block_size);
    printf("AES_256: %d\n", aes_256_block_size);
    printf("3DES: %d\n", des_block_size);

    printf("========= KEY LENGTHS: ===========\n");
    int aes_256_key_len, aes_128_key_len, des_key_len;
    aes_256_key_len = EVP_CIPHER_key_length(EVP_aes_256_cbc());
    aes_128_key_len = EVP_CIPHER_key_length(EVP_aes_128_cbc());
    des_key_len = EVP_CIPHER_key_length(EVP_des_ede3_cbc());

    printf("AES_128: %d\n", aes_128_block_size);
    printf("AES_256: %d\n", aes_256_block_size);
    printf("3DES: %d\n", des_block_size);
    //EVP_CIPHER_CTX * ctx;

}

void handleErrors(void)
{
    ERR_print_errors_fp(stderr);
    abort();
}